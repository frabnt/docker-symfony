Docker - Symfony _(v4)_
==================================

A docker multi-container environment to develop symfony 4 applications. The stack includes:
  
* NGINX 
* PHP7-FPM
* MYSQL
* ELK (Elasticsearch, Logstash and Kibana)
  
---
  
## Dependencies

* Docker engine. See [docs.docker.com/install](https://docs.docker.com/install/)
* Docker compose. See [docs.docker.com/compose/install](https://docs.docker.com/compose/install/)

---

## Setup

* Clone the git repository by running:

        $ git clone https://frabnt@bitbucket.org/frabnt/docker-symfony.git
    
* Copy `.env.dist` into `.env` file.
* Modify the `SOURCE` entry of `.env` file to point to your project.
* Update your system host file by adding:

        127.0.0.1 symfony.localhost
      
* To make your application correctly interacting with the database please update **MySQL** params reflecting the ones 
exposed in docker-compose.yml. 
  
---
  
## How to run

* Firs build docker images, then start docker containers. From the root of cloned repository run:
  
        $ docker-compose build
        $ docker-compose up -d
    
* Install dependencies and create the database:
  
        $ docker-compose exec php-fpm bash
        # Dependencies
        $ composer install
        # Database
        $ sf doctrine:database:create
        $ sf doctrine:schema:update --force
    
* Open a web browser and visit `symfony.localhost`.
